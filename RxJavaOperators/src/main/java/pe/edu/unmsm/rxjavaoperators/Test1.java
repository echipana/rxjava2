/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.rxjavaoperators;

import io.reactivex.Observable;

/**
 *
 * @author Erick
 */
public class Test1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Observable<String> myObservable = Observable.just("a", "b", "c", "d");

        /*suscribirse desconecta el Disposable ascendente cuando recibe un evento terminal, por lo que 
        no se puede eliminar una secuencia de emisión ya completada.Por lo tanto, no se llamará a onDispose */
        myObservable.doOnSubscribe(disposable -> System.out.println("Subscribed!"))
                .doOnDispose(() -> System.out.println("Disposing!"))
                .subscribe(System.out::println);
        System.out.println("-----------------------------------------------------");
        myObservable.doOnSubscribe(disposable -> System.out.println("Subscribed!"))
                .doFinally(() -> System.out.println("Finally!"))
                .subscribe(System.out::println);
        System.out.println("-----------------------------------------------------");
        myObservable.reduce((concat, next) -> concat + next)
                .doOnSuccess(x -> System.out.println(String.format("Emitting: '%s'", x)))
                .subscribe(System.out::println);
        System.out.println("-----------------------------------------------------");

        /*Observable.range(1, 10)
                .doOnNext(r -> getTotal(r))
                .doOnComplete(() -> System.out.println("Completed"))
                .subscribe(System.out::println);*/
    }

    /*public static int getTotal(int numero) {
        return numero * numero;
    }*/

}
