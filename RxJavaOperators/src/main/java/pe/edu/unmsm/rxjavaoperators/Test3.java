/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.rxjavaoperators;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 *
 * @author Erick
 */
public class Test3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        System.out.println("--------------------------------------------");
        Observable.range(1, 5)
                .map(i -> i * 100)
                .doOnNext(i -> {
                    System.out.println("Emitting " + i
                            + " on thread " + Thread.currentThread().getName());
                })
                .subscribeOn(Schedulers.computation())
                .map(i -> i * 10)
                .subscribe(i -> {
                    System.out.println("Received " + i + " on thread "
                            + Thread.currentThread().getName());
                });

        Thread.sleep(3000);
        
        System.out.println("-------------------------------------------------------------");
        Observable.just("First item", "Second item")
                .doOnNext(e -> System.out.println("on-next: Hilo " + Thread.currentThread().getName() + ":" + e))
                .subscribe(e -> System.out.println("subscribe: Hilo " + Thread.currentThread().getName() + ":" + e));
        System.out.println("-------------------------------------------------------------");
        Observable.just("First item", "Second item")
                .subscribeOn(Schedulers.io())
                .doOnNext(e -> System.out.println("on-next: Hilo " + Thread.currentThread().getName() + ":" + e))
                .subscribe(e -> System.out.println("subscribe: Hilo " + Thread.currentThread().getName() + ":" + e));
        Thread.sleep(2000);
    }

}
