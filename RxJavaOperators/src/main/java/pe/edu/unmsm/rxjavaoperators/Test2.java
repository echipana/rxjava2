/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.rxjavaoperators;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 *
 * @author Erick
 */
public class Test2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        Observable.range(1, 5)
                .map(i -> i * 100)
                .doOnNext(i -> {
                    System.out.println("Emitting " + i
                            + " on thread " + Thread.currentThread().getName());
                })
                .observeOn(Schedulers.computation())
                .map(i -> i * 10)
                .subscribe(i -> {
                    System.out.println("Received " + i + " on thread "
                            + Thread.currentThread().getName());
                });

        Thread.sleep(2000);


    }

}
