/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.singleexample;

import io.reactivex.Single;
import java.util.Random;

/**
 *
 * @author Erick
 */
public class Test1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Single.just(getTimeObject()).subscribe(time -> System.out.println("Pasaron " + time + " segundos"));

        Single.fromCallable(() -> getTimeObject())
                .subscribe(time -> System.out.println("Pasaron " + time + " segundos"));

        System.out.println("------------------------------------------");
        Single.just(Math.random()).subscribe(it -> System.out.println("Número: " + it));

        
    
        System.out.println("-----------EJEMPLO JUST -------------");
        //El Single resultante emitirá el mismo Long cuando tenga un nuevo suscriptor
        Single<Double> observableLong = Single.just(Math.random());
        observableLong.subscribe(number -> System.out.println("Numero aleatorio: " + number));
        observableLong.subscribe(number -> System.out.println("Numero aleatorio: " + number));
        observableLong.subscribe(number -> System.out.println("Numero aleatorio: " + number));
        
        //El Single resultante emitirá un Long diferente que indica el tiempo actual en milisegundos cuando tiene un nuevo suscriptor.
        //Eso es porque fromCallable ejecuta su lambda cada vez que tiene un nuevo suscriptor de manera perezosa(Lazily)
        System.out.println("-----------EJEMPLO FROMCALLABLE -------------");
        Single<Double> callableLong = Single.fromCallable(() -> Math.random());
        callableLong.subscribe(number -> System.out.println("Numero aleatorio: " + number));
        callableLong.subscribe(number -> System.out.println("Numero aleatorio: " + number));
        callableLong.subscribe(number -> System.out.println("Numero aleatorio: " + number));
        System.out.println("-----------------------------------------------");
        System.out.println("From Just");
        Single<Double> justSingle = Single.just(getRandomMessage());
        System.out.println("start subscribing");
        justSingle.subscribe(number -> System.out.println("Numero : " + number));

        System.out.println("From Callable");
        Single<Double> callableSingle = Single.fromCallable(() -> getRandomMessage());
        System.out.println("start subscribing");
        callableSingle.subscribe(number -> System.out.println("Numero : " + number));
    }

    public static long getTimeObject() {
        long timeInMillis = System.currentTimeMillis();
        return timeInMillis;
    }

    public static Double getRandomMessage() {
        System.out.println("-Generating-");
        return Math.random();
    }
}
