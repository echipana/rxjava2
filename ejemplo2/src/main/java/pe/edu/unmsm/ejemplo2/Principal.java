/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.ejemplo2;

import io.reactivex.Observable;

/**
 *
 * @author Erick
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Integer[] numbers = {5, 8, 65, 8, 8, 8, 2, 1, 7, 9, 1, 4};

        Observable.fromArray(numbers)
                .filter(x -> x > 4)
                .subscribe(x -> System.out.println("ESTE ES EL NUMERO MAYOR A 4: " + x));
        System.out.println("----------------------------------------");
        Observable.fromArray(numbers)
                .filter(x -> x > 4)
                .lastElement()
                .subscribe(x -> System.out.println("ESTE ES ULTIMO NUMERO MAYOR A 4: " + x));
        System.out.println("----------------------------------------");
        Observable.fromArray(numbers)
                .filter(x -> x > 4)
                .take(3)
                .subscribe(x -> System.out.println("ESTOS SON 3 NUMEROS MAYOR A 4: " + x));

        System.out.println("----------------------------------------");
        Observable.fromArray(numbers)
                .filter(x -> x > 4)
                .count()
                .subscribe(count -> System.out.println("CANTIDAD DE NUMEROS MAYOR A 4: " + count));

        System.out.println("-------LISTA NUMEROS PARES DISTINTOS------");
        //CONVERTIR UN ARRAY DE INTEGERS A STRINGS
        Observable.fromArray(numbers)
                .filter(x -> ((x % 2) == 0)) //filtramos los pares
                .distinct()
                .map(x -> "[" + x + "]")
                .subscribe(System.out::println);
        System.out.println("--------CUENTA NUMEROS PARES-----------");
        Observable.fromArray(numbers)
                .filter(x -> ((x % 2) == 0)) //filtramos los pares
                .count()
                .map(x -> "[" + x + "]")
                .subscribe(System.out::println);
        System.out.println("-------LISTA NUMEROS PARES------------");
        Observable.fromArray(numbers)
                .filter(x -> ((x % 2) == 0)) //filtramos los pares
                //.repeat()
                .map(x -> "[" + x + "]")
                .subscribe(System.out::println);
    }

}
