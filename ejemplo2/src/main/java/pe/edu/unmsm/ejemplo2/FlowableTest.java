/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.ejemplo2;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.Observable;

/**
 *
 * @author Erick
 */
public class FlowableTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Observable<Integer> integerObservable = Observable.just(1, 2, 3, 4);
        Flowable<Integer> integerFlowable = integerObservable.toFlowable(BackpressureStrategy.BUFFER);

        integerFlowable.subscribe(s -> System.out.println("Numero: " + s));

        System.out.println("-----------------------------------------------------");
        FlowableOnSubscribe<Integer> flowableOnSubscribe = flowable -> flowable.onNext(1);
        Flowable<Integer> integerFlowable2 = Flowable.create(flowableOnSubscribe, BackpressureStrategy.BUFFER);
        integerFlowable2.subscribe(s -> System.out.println("Numero: " + s));
    }

}
