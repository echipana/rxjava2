/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.ejemplo3;

import io.reactivex.Observable;

/**
 *
 * @author Erick
 */
public class TestDeferMethod {

    private static int init = 1;
    private static int fin = 5;

    public static void main(String[] args) {
       
        //DEFER : no crea el Observable hasta que el observador se suscriba y crea un Observable nuevo para cada observador
        Observable<Integer> source = Observable.defer(()->Observable.range(init, fin));
        
        source.subscribe(i->System.out.println("Observer 1:"+i));        
        fin = 10;        
        source.subscribe(i->System.out.println("Observer 2:"+i));
    }

}
