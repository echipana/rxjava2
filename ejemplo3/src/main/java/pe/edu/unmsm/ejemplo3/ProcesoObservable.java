/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.ejemplo3;

import io.reactivex.Observable;
import io.reactivex.Observer;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Erick
 */
public class ProcesoObservable extends Observable<String> {

    private List<Observer<? super String>> misObservadores = new LinkedList<>();
    private String mensaje;

    public ProcesoObservable(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    protected void subscribeActual(Observer<? super String> obsrvr) {
        misObservadores.add(obsrvr);

    }

    public void enviarMensaje() {

        //retrasamos el mensaje 4 segundos a modo de prueba
        Timer timerMensaje = new Timer();
        timerMensaje.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
              misObservadores.forEach(observador -> observador.onNext(mensaje));
              misObservadores.forEach(observador -> observador.onComplete());
              timerMensaje.cancel();
            }
        }, 4000, 1);

    }

}
