/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.ejemplo3;

import io.reactivex.Observable;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Erick
 */
public class Test2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {

        System.out.println("--------EJEMPLO CREATE-------");
        Observable<String> source = Observable.create(emitter -> {
            try {
                emitter.onNext("Uno");
                emitter.onNext("Dos");
                emitter.onNext("Tres");
                emitter.onComplete();
            } catch (Throwable e) {
                emitter.onError(e);
            }
        });

        source.subscribe(s -> System.out.println("Numero recibido: " + s),
                Throwable::printStackTrace);

        System.out.println("-------EJEMPLO JUST--------");
        Observable<String> source2 = Observable.just("Uno", "Dos", "Tres");
        source2.subscribe(s -> System.out.println("Numero recibido: " + s));

        System.out.println("-------EJEMPLO FROM ITERABLE--------");
        List<String> items = Arrays.asList("Uno", "Dos", "Tres");
        Observable<String> source3 = Observable.fromIterable(items);
        source3.subscribe(s -> System.out.println("Numero recibido: " + s));

        System.out.println("-------EJEMPLO RANGE--------");
        Observable.range(1, 10).subscribe(s -> System.out.println("Rango recibido: " + s));

        System.out.println("-------EJEMPLO INTERVAL--------");
        Observable.interval(1, TimeUnit.SECONDS).subscribe(s -> System.out.println(s + "Mississippi"));
        Thread.sleep(10000);
    }

}
