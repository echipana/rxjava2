/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.ejemplo3;

/**
 *
 * @author Erick
 */
public class Test1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String mensaje = "Aprendiendo observer";
        ProcesoObservable proceso = new ProcesoObservable(mensaje);
        //se ejecuta cuando el observador recibe la notificacion del proceso observable
        proceso.subscribe(mensajeRecibido -> {
            System.out.println(mensajeRecibido);
        });

        proceso.enviarMensaje();

        otrosProcesos();
        System.out.println("Esperando que terminen procesos pendientes");

    }

    private static void otrosProcesos() {

        int a = 0;
        for (int i = 0; i < 10000; i++) {
            a += i;
        }
    }
}
