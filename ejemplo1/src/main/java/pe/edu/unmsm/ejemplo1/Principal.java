/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.ejemplo1;

import io.reactivex.Observable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Erick
 */
public class Principal {

    private List<Integer> lista1;
    private List<Integer> lista2;

    public Principal() {
        lista1 = new ArrayList<>();
        lista2 = new ArrayList<>();
        this.llenarListas();
    }

    public void llenarListas() {

        for (int i = 0; i < 10; i++) {
            lista1.add(i);
            lista2.add(i);
        }
    }

    public void buscar() {
        Observable<Integer> obs1 = Observable.fromIterable(lista1);
        Observable<Integer> obs2 = Observable.fromIterable(lista2);
        //Observable.merge(obs1, obs2).filter(elemento -> elemento == 1).subscribe(elemento -> System.out.println(elemento));

        //subscribe con metodo de referencia
        Observable.merge(obs1, obs2).filter(elemento -> elemento == 1).subscribe(System.out::println);
        
       /* Observable.merge(obs1, obs2).filter(elemento -> elemento == 1).subscribe(x -> {
            if (x == 1) {
                System.out.println(x);
            }
        });*/
    }

    public static void main(String[] args) {

        Integer[] numbers = {10, 3, 1, 5, 8, 65, 8, 2, 1, 7, 9, 6, 4};
        Observable.fromArray(numbers)
                .filter(number -> number > 4)
                .firstElement()
                .subscribe(number -> System.out.println("Este es el primer numero mayor a 4 : " + number));

        System.out.println("-----------Ejemplo 1 ----------------- ");
        List<String> lista = new ArrayList<>();
        lista.add("Erick");
        lista.add("Chipana");
        lista.add("Erick Chipana");
        Observable<String> obs = Observable.fromIterable(lista);
        obs.subscribe(elemento -> System.out.println(elemento));

        System.out.println("-----------Ejemplo 2 ----------------- ");
        Principal principal = new Principal();
        principal.buscar();
    }

}
