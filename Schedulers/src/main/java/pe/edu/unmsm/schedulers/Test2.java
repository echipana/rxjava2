/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.schedulers;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import java.util.Random;

/**
 *
 * @author Erick
 */
public class Test2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        Observable.just("A", "AB", "ABC")
                .flatMap(v -> getLengthWithDelay(v)
                .doOnNext(s -> System.out.println("Processing Thread "
                + Thread.currentThread().getName()))
                .subscribeOn(Schedulers.computation()))
                .subscribe(length -> System.out.println("Receiver Thread "
                + Thread.currentThread().getName()
                + ", Item length " + length));

        Thread.sleep(10000);
    }

    protected static Observable<Integer> getLengthWithDelay(String v) {
        Random random = new Random();
        try {
            Thread.sleep(random.nextInt(3) * 1000);
            return Observable.just(v.length());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

}
