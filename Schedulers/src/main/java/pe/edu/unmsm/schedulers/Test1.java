/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.schedulers;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 *
 * @author Erick
 */
public class Test1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {

        Observable.just("long", "longer", "longest")
                .map(String::length)
                .subscribe(length -> System.out.println("item length " + length));
        System.out.println("-----------------------------------------------------");
        Observable.just("long", "longer", "longest")
                .doOnNext(c -> System.out.println("processing item on thread " + Thread.currentThread().getName()))
                .map(String::length)
                .subscribe(length -> System.out.println("item length " + length));
        System.out.println("-------------Schedulers.newThread()-------");
        Observable.just("long", "longer", "longest")
                .doOnNext(c -> System.out.println("processing item on thread " + Thread.currentThread().getName()))
                .subscribeOn(Schedulers.newThread())
                .map(String::length)
                .subscribe(length -> System.out.println("item length " + length));
        Thread.sleep(3000);
        
           System.out.println("-------------Schedulers.computation()-------");
        Observable.just("long", "longer", "longest")
                .doOnNext(s -> System.out.println("processing item on thread " + Thread.currentThread().getName()))
                .subscribeOn(Schedulers.computation())
                .map(String::length)
                .subscribe(length -> System.out.println("item length " + length));
        Thread.sleep(3000);
    }

}
